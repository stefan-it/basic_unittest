# This file is part of basic_unittest.
#
# Copyright 2015-2016 Stefan Schweter
#
# basic_unittest is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SHELL = /bin/bash
MAKESHELL = /bin/bash

GIT ?= git
HEAD_REV = $(shell $(GIT) rev-parse --short=5 HEAD)

CXX ?= g++
CXXFLAGS ?= -std=c++11 -Ofast -Wall -Wextra -Wpedantic

EXEC ?= basic_unittest_example
SRCS = $(wildcard *.cpp)
OBJS = $(SRCS:.cpp=.o)

CLEANUP  = $(patsubst %, %.cleanup, $(shell find -type f -iname \*.o))
CLEANUP += $(patsubst %, %.cleanup, $(shell find -type f -name $(EXEC)))

SILENT ?= @
QUIET ?= 2>&1 >/dev/null

.PHONY: default
default: all

.PHONY: all
all: bin

.PHONY: bin
bin: $(EXEC)

$(EXEC): $(OBJS)
	$(SILENT) $(CXX) $(OBJS) -o $(EXEC)

%.o: %.cpp
	$(SILENT) $(CXX) -c $(CXXFLAGS) $< -o $@

.PHONY: dist
dist:
	$(SILENT) $(GIT) archive --format tar.gz --prefix=basic_unittest/ \
	--output basic_unittest_$(HEAD_REV).tar.gz HEAD

.PHONY: clean
clean: $(CLEANUP)

%.cleanup: %
	$(SILENT) $(RM) $<

.PHONY: distclean
distclean: clean
	$(SILENT) $(GIT) clean -fdqx -f
