/*
 * This file is part of basic_unittest.
 *
 * Copyright 2015-2016 Stefan Schweter
 *
 * basic_unittest is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file basic_unittest.hpp
 *  @brief Implements a small and lightweight unittest framework
 *
 * This header includes all macros for implementing a small and lightweight
 * unittest framework for C++11.
 *
 * The following macros can be uses:
 *
 * - BEGIN_TEST_SUITE() - Declaration for a beginning test suite
 * - END_TEST_SUITE()   - Declaration for finishing a test suite
 * - BEGIN_TEST()       - Declaration for a beginning test case
 * - END_TEST()         - Declaration for finishing a test case
 *
 * The following macros for checking several conditions can be used to test if
 * a condition is met:
 *
 * - EXPECT_TRUE(condition)        - Checks if a condition is true
 * - EXPECT_FALSE(condition)       - Checks if a condition is false
 * - EXPECT_EQUAL(left, right)     - Checks if two values are equal
 * - EXPECT_NOT_EQUAL(left, right) - Checks if two values are not equal
 *
 * @author Stefan Schweter <stefan@schweter.it> (2016)
 * @license This project is released under the GNU General Public License
 *          version 3, see https://www.gnu.org/licenses/gpl-3.0.html
 */

#include <unistd.h>

int suite_test_count = 0;
int suite_test_successful = 0;
int suite_test_failed = 0;

int test_count = 0;
int test_successful = 0;
int test_failed = 0;

enum test_case_states : bool { SUCCESSFULL = 1, FAILED = 0 };
test_case_states test_case_state;

#define PRINT_NUMBER_OF_TESTS(test_case_state, number_of_tests)\
  {\
    if (number_of_tests > 0) {\
      switch(test_case_state) {\
        case SUCCESSFULL:\
          if (isatty(fileno(stdout))) {\
            fprintf(stdout, "\033[32;1m %d \033[0m", number_of_tests);\
          } else {\
            fprintf(stdout, " %d ", number_of_tests);\
          }\
          break;\
        case FAILED:\
          if (isatty(fileno(stdout))) {\
            fprintf(stdout, "\033[31;1m %d \033[0m", number_of_tests);\
          } else {\
            fprintf(stdout, " %d ", number_of_tests);\
          }\
          break;\
      }\
    } else {\
      fprintf(stdout, " %d ", number_of_tests);\
    }\
  }

#define BEGIN_TEST_SUITE(test_suite_name)\
  suite_test_count = 0;\
  suite_test_successful = 0;\
  suite_test_failed = 0;\
  {\
    fprintf(stdout, "Running test suite %s\n", test_suite_name);\
  }

#define BEGIN_TEST(test_name)\
  test_count = 0;\
  test_successful = 0;\
  test_failed = 0;\
  {\
    fprintf(stdout, "↳ Running test ◜%s◞ for ｢%s｣\n", test_name, __FILE__);\
  }

#define EXPECT_TRUE(condition)\
  { ++test_count;\
    if (condition) {\
      ++test_successful;\
    } else {\
      ++test_failed;\
      fprintf(stderr, "  ☒ Condition NOT met in line %d\n", __LINE__);\
    }\
  }

#define EXPECT_FALSE(condition)\
  { ++test_count;\
    if (!condition) {\
      ++test_successful;\
    } else {\
      ++test_failed;\
      fprintf(stderr, "  ☒ Condition NOT met in line %d\n", __LINE__);\
    }\
  }

#define EXPECT_EQUAL(left, right)\
  { ++test_count;\
    if (left == right) {\
      ++test_successful;\
    } else {\
      ++test_failed;\
      fprintf(stderr, "  ☒ Values are NOT equal in line %d\n", __LINE__);\
    }\
  }

#define EXPECT_NOT_EQUAL(left, right)\
  { ++test_count;\
    if (left != right) {\
      ++test_successful;\
    } else {\
      ++test_failed;\
      fprintf(stderr, "  ☒ Values ARE equal in line %d\n", __LINE__);\
    }\
  }

#define END_TEST()\
  { suite_test_count += test_count;\
    suite_test_successful += test_successful;\
    suite_test_failed += test_failed;\
    fprintf(stdout, "  ↳ PASSED: ");\
    PRINT_NUMBER_OF_TESTS(SUCCESSFULL, test_successful);\
    fprintf(stdout, " test(s), FAILED: ");\
    PRINT_NUMBER_OF_TESTS(FAILED, test_failed);\
    fprintf(stdout, " test(s)\n");\
  }

#define END_TEST_SUITE()\
  {\
    fprintf(stdout, "%d tests in test suite executed: ", suite_test_count);\
    PRINT_NUMBER_OF_TESTS(SUCCESSFULL, suite_test_successful);\
    fprintf(stdout, " passed, ");\
    PRINT_NUMBER_OF_TESTS(FAILED, suite_test_failed);\
    fprintf(stdout, " failed.\n");\
  }
