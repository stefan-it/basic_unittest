# Changelog

## 1.0.1 (2016-09-23)

* Contribution and coding style guidelines added (see 773abb69 and c32e124c)

* Project was moved to [GitLab.com](https://gitlab.com/stefan-it/basic_unittest)

* Makefile for building the example program added (see de77de9c)

## 1.0.0 (2015-07-30)

* Initial version
