# `basic_unittest`

Implements a small and lightweight unittest framework written for C++11.

## Build status

[![build status](https://gitlab.com/stefan-it/basic_unittest/badges/master/build.svg)](https://gitlab.com/stefan-it/basic_unittest/commits/master)

## Macros

The following macros can be used:

| Macro name | Description
| -------- | --------
| `BEGIN_TEST_SUITE(test_suite_name)` |  Declaration for a beginning test suite
| `END_TEST_SUITE()` | Declaration for finishing a test suite
| `BEGIN_TEST(test_name)` |  Declaration for a beginning test case
| `END_TEST()` | Declaration for finishing a test case

### Macros for checking several conditions

The following `EXPECT_*` macros can be used to test if a condition is met:

| Macro name | Description
| -------- | --------
| `EXPECT_TRUE(condition)` | Checks if a condition is `true`
| `EXPECT_FALSE(condition)` | Checks if a condition is `false`
| `EXPECT_EQUAL(left, right)` | Checks if two values are equal
| `EXPECT_NOT_EQUAL(left, right)` | Checks if two values are *not* equal

# Header-driven framework

To use this basic unittest framework just include its header file:

```cpp
#include "basic_unittest.hpp"
```

# Example usage

Here's a small example for a test suite and test cases:

```cpp
#include <string>
#include <cwctype>
#include "basic_unittest.hpp"

auto main() -> int {
  BEGIN_TEST_SUITE("Test basic unittest framework");
    BEGIN_TEST("POD tests");
      int i = 5;
      int j = 4 + 1;

      EXPECT_EQUAL(i, j);

      bool b1 = false;
      bool b2 = true;

      EXPECT_NOT_EQUAL(b1, b2);

      signed char c1 = 'A';
      signed char c2 = 'A';

      EXPECT_EQUAL(c1, c2);
    END_TEST();
  END_TEST_SUITE();

  return 0;
}
```

# Output

The unittest framework outputs a nice result result of the executed test suite
and its test cases:

```bash
Running test suite Test basic unittest framework
↳ Running test ◜Expect_true test◞ for ｢main.cpp｣
  ↳ PASSED:  1  test(s), FAILED:  0  test(s)
↳ Running test ◜More complex tests◞ for ｢main.cpp｣
  ↳ PASSED:  5  test(s), FAILED:  0  test(s)
↳ Running test ◜POD tests◞ for ｢main.cpp｣
  ↳ PASSED:  3  test(s), FAILED:  0  test(s)
9 tests in test suite executed:  9  passed,  0  failed.
```

If an error occured the output is:

```bash
Running test suite Test basic unittest framework
↳ Running test ◜Expect_true test◞ for ｢example.cpp｣
  ↳ PASSED:  1  test(s), FAILED:  0  test(s)
↳ Running test ◜More complex tests◞ for ｢example.cpp｣
  ☒ Values are NOT equal in line 25
  ☒ Values are NOT equal in line 40
  ↳ PASSED:  3  test(s), FAILED:  2  test(s)
↳ Running test ◜POD tests◞ for ｢example.cpp｣
  ☒ Values are NOT equal in line 59
  ↳ PASSED:  2  test(s), FAILED:  1  test(s)
9 tests in test suite executed:  6  passed,  3  failed.
```

# Dependencies

The basic unittest framework was tested under:

* *GCC* version *5.1* and the development version of *6.0*
* *clang* version *3.6.1*

But it should compile with older compiler versions (C++11 support is needed).

# Compilation of `example.cpp`

To compile the basic unittest example file (located in `example.cpp`) just
execute:

```bash
make
```

To use `clang` as compiler the `CXX` variable can be modified:

```bash
make CXX=clang++
```

# License

The unittest framework is distributed under the *GNU General Public License*
version *3* to show respect to Richard Stallman and the Free Software
Movement. The *GPLv3* license is located in `COPYING` or can be viewed on
the *GNU* website [here](https://www.gnu.org/licenses/gpl-3.0.html).

# Contact (Bugs, Feedback, Contribution and more)

For questions about *basic_unittest*, contact the current maintainer:
Stefan Schweter <stefan@schweter.it>. If you want to contribute to the project
please refer to the [Contributing](CONTRIBUTING.md) guide!
