/*
 * This file is part of basic_unittest.
 *
 * Copyright 2015-2016 Stefan Schweter
 *
 * basic_unittest is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file example.hpp
 *  @brief Implements some examples for the basic_unittest usage
 *
 * @author Stefan Schweter <stefan@schweter.it> (2016)
 */

#include <string>
#include <cwctype>
#include "basic_unittest.hpp"

auto main() -> int {
  std::setlocale(LC_ALL, "");

  BEGIN_TEST_SUITE("Test basic unittest framework");

    BEGIN_TEST("Expect_true test");

      EXPECT_TRUE(true);

    END_TEST();

    BEGIN_TEST("More complex tests");

      EXPECT_FALSE(false);

      std::wstring w1(L"Hallo");
      std::wstring w2(L"hallo");

      w2.at(0) = std::towupper(w2.at(0));

      EXPECT_EQUAL(w1, w2);

      std::wstring w3(L"Über");
      std::wstring w4(L"über");

      EXPECT_NOT_EQUAL(w3, w4);

      std::wstring w5(L"öäöä");
      std::wstring w6(L"♠");

      EXPECT_NOT_EQUAL(w5, w6);

      std::wstring w7(L"☻");
      std::wstring w8(L"☻");

      EXPECT_EQUAL(w7, w8);

    END_TEST();

    BEGIN_TEST("POD tests");

      int i = 5;
      int j = 4 + 1;

      EXPECT_EQUAL(i, j);

      bool b1 = false;
      bool b2 = true;

      EXPECT_NOT_EQUAL(b1, b2);

      signed char c1 = 'A';
      signed char c2 = 'A';

      EXPECT_EQUAL(c1, c2);

    END_TEST();

  END_TEST_SUITE();

  return 0;
}
